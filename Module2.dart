/*---MTN APP ACADEMY MODULE 2---
  CODE BY: AUSTIN MOFOKENG
  DATE: 29 MAY 2022
*/
void main(List<String> args) {
  /*========Part_1========*/
  print("========Part_1========");
  var name = "Austin";
  var favApp = "Slack";
  var city = "Heidelburg";

  print("Name: $name");
  print("Favorite app: $favApp");
  print("City: $city");
  print("\n");

  /*=========Part_2========*/
  print("========Part_2========");

  var WinningApps = [
    "FNB Banking App\n",
    "Bookly - Track Books and Reading stats\n",
    "SuperSport\n",
    "Dstv NOW\n",
    "hearZA\n",
    "Oderin Driver\n",
    "ASI Snakes\n",
    "Hydra\n",
    "Checkers Sixty60\n",
    "Takealot - Online Shopping App\n"
  ];

  sorting(WinningApps);
  print(WinningApps);

  print("winning app of 2017 : ${WinningApps[6]}");
  print("Winning app of 2018 : ${WinningApps[0]}");
  print("Number of elements in array: ${WinningApps.length}");
  print("\n");

  /*=========*Part_3==========*/
  print("========Part_3=========");
  Academy app1 = new Academy();

  print("App name: ${app1.app}"); // Use of object to print output
  print("Developer: ${app1.developer}");
  print("Sector: ${app1.sector}");
  print("Year the app won: ${app1.yearWon}");

  print("App name in capital: ${app1.capital(app1.app)}");
}

/*=========Part_2=========*/
void sorting(List<String> WinningApps) {
  WinningApps.sort((a, b) {
    return a.compareTo(b);
  }); //Function to sort the array alphabetically
}

/*=========Part_3=========*/
class Academy {
  //class creation
  String app = "Takealot";
  String sector = "eCommerce";
  String developer = "Kim Raider";
  int yearWon = 2021;

  String capital(String app) {
    return (app.toUpperCase()); //Function to capitalize letters
  }
}
